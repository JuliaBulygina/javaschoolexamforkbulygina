package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        int[][] result = buildingPyramid(list);
        if(result != null)
            return result;
        else
            return CannotBuildPyramidException;
    }

    private static int[][] buildingPyramid(List<Integer> inputNumbers){
        //System.out.println(inputNumbers);
        List<Integer> sortedList = new ArrayList<>();
        sortedList = sorter(inputNumbers);
        //System.out.println(sortedList);
        List<List<Integer>> numbers = new ArrayList<List<Integer>>();
        int countRow = 1;
        int countIndexDone = 0;
        int countIndex = 0;
        for (int i = 0; i < countRow; i++){
            numbers.add(new ArrayList<>());
            do{
                numbers.get(i).add(countIndex - countIndexDone, sortedList.get(countIndex));
                countIndex++;
                if(countIndex == sortedList.size()) break;
            } while (countIndex - countIndexDone < countRow);
            countIndexDone += countRow;
            countRow++;
            if(countIndex == sortedList.size()) break;
        }
        //System.out.println(numbers);
        int[][] result = new int[numbers.size()][numbers.get(numbers.size() -1).size()*2-1];
        if (numbers.get(numbers.size()- 1).size() <= numbers.get(numbers.size()- 2).size())
            //result = null;
            return null;
        int countTabs =0;
        for(int i = numbers.size() - 1; i >=0; i--){
            countIndex = 0;
            for(int j = 0; j < result[i].length; j ++){
                if(j >= countTabs & j < result[i].length - countTabs) {
                    if (countTabs%2 == 0 & j%2 == 0) {
                        result[i][j] = numbers.get(i).get(countIndex);
                        countIndex++;
                    }
                    if (countTabs%2 == 1 & j%2 == 1) {
                        result[i][j] = numbers.get(i).get(countIndex);
                        countIndex++;
                    }
                }
            }
            countTabs++;
        }
        return result;
    }

    private static List<Integer> sorter(List<Integer> list){
        boolean sorted = false;
        int temp;
        while(!sorted) {
            sorted = true;
            for (int i = 0; i < list.size() - 1; i++) {
                if (list.get(i) > list.get(i+1)) {
                    temp = list.get(i);
                    list.set(i, list.get(i+1));
                    list.set(i+1, temp);
                    sorted = false;
                }
            }
        }
        return list;
    }
}